from flask import jsonify, request, url_for, g, abort
from app.api.auth import token_auth

from app import db
from app.api.errors import bad_request
from app.models import Post


@bp.route('/post/<int:id>', methods=['GET'])
def get_post(id):
    return jsonify(Post.query.get_or_404(id).to_dict())


@bp.route('/posts', methods=['GET'])
def get_posts():
    """
    TODO: Make it work.
    """
    page = request.args.get('page', 1, type=int)
    per_page = min(request.args.get('per_page', 10, type=int), 100)
    data = User.to_collection_dict(User.query, page, per_page, 'api.get_users')
    return jsonify(data)


@bp.route('/posts', methods=['POST'])
@token_auth.login_required
def create_post():
    """
    TODO: Make it work.
    """
    data = request.get_json() or {}
    if 'username' not in data or 'password' not in data:
        return bad_request('must include username and password fields')
    if Post.query.filter_by(username=data['username']).first():
        return bad_request('please use a different username')
    post = Post()
    post.from_dict(data, new_post=True)
    db.session.add(user)
    db.session.commit()
    response = jsonify(post.to_dict())
    response.status_code = 201
    response.headers['Location'] = url_for('api.get_post', id=post.id)
    return response


@bp.route('/posts', methods=['POST'])
def get_top_posts():
    """
    TODO: Make it work.
    """
    pass