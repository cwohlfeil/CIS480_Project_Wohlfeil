import rq
from PIL import Image
from cityhash import CityHash64

from app import app, db
from app.models import Post


# TODO: RQ
def optimize_image(filename):
    """
    TODO: Get filepath from config, not cwd
          Quality from config, not hardcode
          Make work with PNG, GIF
    """
    filepath = os.path.join(os.getcwd(), filename)
	picture = Image.open(filepath)
	
	#set quality = to the preferred quality. 
	# I found that 85 has no difference and 65 is the lowest reasonable number
	picture.save(filename,"JPEG", optimize=True, quality=85) 
    pass


# TODO: RQ
def rename_image(file):
    """
    TODO: Get filepath from config, not cwd
          Get naming scheme from config
    """
    filepath = os.path.join(os.getcwd(), filename)


# TODO: RQ
def hash_and_check_image(file):
    """
    TODO: Hash image with CityHash64
    """
    hash = CityHash64(file)
    
    for post in Post:
        if post.image_hash == hash:
            file = post.image_path
    